package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        observable.addObserver(this);
        /* Requires an observable subject to initialize, and subscribing to it. Since all its data comes from
         * the subject anyways and all of its data has been initialized with dummy values, the initialization needs no
         * this.maxTemp = maxTemp; and its not a problem.
         */
    }

    @Override //Just to notify that this method overrides another method, just to help the IDE make sure we dont have mistakes
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData) o;
            this.tempSum += ((WeatherData) o).getTemperature();
            numReadings++;
            if(((WeatherData) o).getTemperature() > this.maxTemp){
                this.maxTemp = ((WeatherData) o).getTemperature();
            }
            if(((WeatherData) o).getTemperature() < this.minTemp){
                this.minTemp = ((WeatherData) o).getTemperature();
            }

            display();
            // update just happens
        }
    }
}
