package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    QuackBehavior quackBehavior = new Squeak();
    FlyBehavior flyBehavior = new FlyNoWay();

    public void display(){
        System.out.println("I wanna be a real duck!");
    }
    // TODO Complete me!
}
