package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {
    // TODO Complete me!
    @Override//Annotation, used to notify that this one below overrides something
    public void fly() {
        System.out.println("Grounded.png");
    }
}
