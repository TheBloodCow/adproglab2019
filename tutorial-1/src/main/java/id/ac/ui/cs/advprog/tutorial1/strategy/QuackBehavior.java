package id.ac.ui.cs.advprog.tutorial1.strategy;

public interface QuackBehavior {

    void quack();//We just want to implement quack
}
