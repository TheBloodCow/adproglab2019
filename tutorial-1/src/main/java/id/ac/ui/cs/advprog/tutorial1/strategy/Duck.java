package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    /*
    The above are important to note because they take the form of a HAS-A function instead of an IS-A function, meaning
    that these behaviors are encapsulated and can also support a potentially unlimited amount of changes

    1. Identify the constants and the variables
    2. Make strategy interfaces for the behaviors that vary
    3. Make classes that implement the interfaces
    4. Reflect the above changes in the main method
     */

    //Since the variable methods are in a private behavior, it must be able to be set.

    public void setFlyBehavior(FlyBehavior newFlyBehavior){
        this.flyBehavior = newFlyBehavior;
    }

    public void setQuackBehavior(QuackBehavior newQuackBehavior){
        this.quackBehavior = newQuackBehavior;
    }

    public abstract void display(); //Customer request is that each duck be described (a bit wasteful tbh)

    public void swim(){ System.out.println("Swoosh swoosh");}
    // This is constant across all forms of ducks, so it can simply be inherited by every duck

    // TODO Complete me!
}
