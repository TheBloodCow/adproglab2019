package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    QuackBehavior quackBehavior = new Quack();
    FlyBehavior flyBehavior = new FlyWithWings();
    public void display(){
        System.out.println("I'm a Mallard Duck");
    }
}
