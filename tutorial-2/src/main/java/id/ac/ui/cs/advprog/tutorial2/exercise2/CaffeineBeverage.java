package id.ac.ui.cs.advprog.tutorial2.exercise2;

public abstract class CaffeineBeverage {

    public final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        addCondiments();
    }

    protected abstract void addCondiments();

    protected abstract void brew();

    public void boilWater() {
        System.out.println("Boiling water");
    }

    public void pourInCup() {
        System.out.println("Pouring into cup");
    }
}

/*
    Template is compile-time subclass
    Strategy is runtime encapsulation/abstraction

    Template & Strategy:
    - No code dupe
    - Open for Extension Closed for Modification

    Template:
    - Algorithm needs to know the internal aspects of the object it runs on

    Strategy:
    - Only implementable for algorithms
    - Very private
 */
