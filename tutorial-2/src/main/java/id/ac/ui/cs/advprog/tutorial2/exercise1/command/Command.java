package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

public interface Command {
//Command Template -> All commands must have these two methods so that the remote can blindly execute or undo
/*Command pattern is used to encapsulate a request as an object, parameterizing clients, requests, logs,
    operations and other so different things. It binds requests, operations and other similar actions that would
    otherwise need different execution classes into one.
 */
    void execute();

    void undo();
}
