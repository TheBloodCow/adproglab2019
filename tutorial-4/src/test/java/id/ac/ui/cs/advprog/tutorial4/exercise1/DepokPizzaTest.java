package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaTest {
    private Pizza testPizza;
    private PizzaStore pizzaStore;
    private Dough dough;
    private PizzaIngredientFactory factory;
    private Sauce sauce;
    private Veggies veggies;
    private Clams clam;
    private PizzaStore store;


    @Before
    public void setUp() {
        store = new DepokPizzaStore();
        factory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void factoryTest(){
        assertEquals(factory.createDough().toString(),"Thin Crust Dough");
        assertEquals(factory.createSauce().toString(),"Crushed San Marzano Tomato Sauce");
        assertEquals(factory.createCheese().toString(),"Smoked Gouda");
    }
    @Test
    public void cheesePizza() {
        testPizza = store.orderPizza("cheese");
        assertEquals(testPizza.getName(), "Amal Style Pizza because Depok makes no cheese");

    }

    @Test
    public void clamPizza() {
        testPizza = store.orderPizza("clam");
        assertEquals(testPizza.getName(), "Depok Style clam pizza");
    }

    @Test
    public void veggiePizza() {
        testPizza = store.orderPizza("veggie");
        assertEquals(testPizza.getName(), "Depok Style Garlic Pizza");
    }

    @Test(expected = IllegalArgumentException.class)
    public void failPizza() {
        testPizza = store.orderPizza("PilihPaslon#01");
    }
}