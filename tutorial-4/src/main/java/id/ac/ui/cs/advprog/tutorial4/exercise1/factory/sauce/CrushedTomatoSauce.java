package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class CrushedTomatoSauce implements Sauce {
    public String toString(){return "Crushed San Marzano Tomato Sauce";}
}
