package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    private static Singleton thisOne;
    //private static Singleton thisOne = new Singleton
    /*
     A static attribute of its own type to ensure that this is always the right instance
     But this is for the lazy type of Singleton. What happens if you want an eager type?
      */
    /*
        There are two types of Singletons, lazy and eager
        Since I am mahasiswa, I will implement the lazy Singleton here and
        comment out the eager Singleton

        Lazy: If it is never instantiated, the object will never exist.
        Good thing? It doesn't exist when you don't use it and creation and runtime
        overhead is avoided when not in use.
        The bad thing about this? You run into multithreading problems, which
        basically all machines have at this point.

        OH! BUT NOT TO WORRY: YOU CAN SIMPLY type Synchronized AFTER STATIC IN THE
        GETINSTANCE AND ALL IS SOLVED FOR THE LOW, LOW PRICE OF OVERHEAD FOR THE REST OF
        THE CODE! Synchronized is only used for the one time we instantiate it.

        Eager: It's automatically instantiated and the object will always exist.
        Good Thing? Avoids multithreading problems and ensures once and for all that there
        is only one of this object ever.
        Bad thing? If the creation and runtime overhead is big, expect your program to bloat
     */

    private Singleton(){}
    /*
     A constructor that can only be accessed by Singleton itself so as to make sure
     that it can only be made once.
      */

    public static Singleton getInstance() {
        /*
        Eager:
        return thisOne
        */
        /*
        Yeah that's right, with Eager, there is always a singleton so there's no point in
        checking.
         */
        if(thisOne==null){
            thisOne = new Singleton();
        }
        return thisOne;
    }
}
